-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2018 at 02:51 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `user_identification`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `email`, `password`, `created`) VALUES
(15, 'Diwanee1', 'Diwanee1', 'Diwanee1@diwanee1.com', 'Diwanee1', '2018-09-16 03:52:36'),
(16, 'Diwanee2', 'Diwanee2', 'Diwanee2@diwanee2.com', 'Diwanee2', '2018-09-16 03:54:00'),
(17, 'Diwanee3', 'Diwanee3', 'Diwanee3@diwanee3.com', 'Diwanee3', '2018-09-16 04:15:01'),
(18, 'Diwanee4', 'Diwanee4', 'Diwanee4@diwanee4.com', 'Diwanee4', '2018-09-16 14:02:16'),
(19, 'Diwanee5', 'Diwanee5', 'Diwanee5@diwanee5.com', 'Diwanee5', '2018-09-16 14:53:53'),
(20, 'Diwanee6', 'Diwanee6', 'Diwanee6@diwanee6.com', 'Diwanee6', '2018-09-16 19:04:41'),
(21, 'Diwanee7', 'Diwanee7', 'Diwanee7@diwanee7.com', 'Diwanee7', '2018-09-16 19:33:48'),
(22, 'Diwanee8', 'Diwanee8', 'Diwanee8@diwanee8.com', 'Diwanee8', '2018-09-18 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
