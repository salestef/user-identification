<?php

class Bo extends Controller
{
    /**
     * Default method in Bo Controller.
     */
    public function index()
    {
        $this->view('bo/index', [], 'bo', [], 'bo');
    }

    /**
     * Home method in Bo Controller.
     * Render home view.
     */
    public function home()
    {
        $this->view('bo/index', [], 'bo', [], 'bo');
    }

    /**
     * Users method in Bo Controller.
     * Renders specific user view and pass list of users in descending time order like parameter.
     */
    public function users()
    {
        $this->view('bo/users', ['users' => $this->model('User')->getAllUsers()], 'bo', [], 'bo');
    }


}