<?php

class Users extends BaseController
{

    /**
     * Default users controller method renders login view.
     */
    public function index()
    {
        $this->view('users/login');
    }


    /**
     * Used for user login.
     * @return bool
     */
    public function login()
    {
        // Is User all ready logged
        if (Auth::isLoggedIn()){
            header("Location: " . URL_PROJECT_PATH . "/bo");
        }

        // Are all params from the form properly sent to the method
        if (!isset($_POST['email']) || !isset($_POST['password'])) {
            $this->view('users/login');
            return true;
        }

        // Init User model
        $user = $this->model('User');

        // Check does the user with these credentials already exist and get the User if so
        $userLogged = $user->login($_POST['email'], $_POST['password']);

        if (!$userLogged) {
            $this->view('users/login');
            return true;
        }

        // Check data returned from User model login method and fill the session with user data
        if ($userLogged instanceof User) {
            $_SESSION['user'] = [
                'id' => $userLogged->id,
                'name' => $userLogged->name,
                'lastName' => $userLogged->lastName,
                'email' => $userLogged->email,
                'created' => $userLogged->created,
            ];
        }

        // If everything is ok go to Back Office if not return to login view
        if (isset($_SESSION['user'])) {
            $this->view('bo/index', [], 'bo', [], 'bo');
        } else {
            $this->view('users/login');
        }
    }

    /**
     * Logout the user and send him to application starting page.
     */
    public function logout()
    {
        if (isset($_SESSION['user'])) {
            session_destroy();
        }
        header("Location: " . URL_PROJECT_PATH);
    }

    /**
     * User registration.
     * @return bool
     */
    public function register()
    {
        // Is User all ready logged send him to Back Office
        if (Auth::isLoggedIn()){
            header("Location: " . URL_PROJECT_PATH . "/bo");
        }

        // Are all params from the form properly sent to the method
        if (!isset($_POST['email']) || !isset($_POST['password']) || !isset($_POST['name']) || !isset($_POST['password'])) {
            $this->view('users/register');
            return true;
        }

        // Init User model
        $user = $this->model('User');

        // Register new user and if success use login method for instant user login, and go to Back Office
        $userRegistered = $user->register($_POST['email'], $_POST['password'],$_POST['name'], $_POST['lastName']);
        if($userRegistered){
            $this->login($_POST['email'], $_POST['password']);
        }
    }


}