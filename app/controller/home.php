<?php

class Home extends BaseController {

    /**
     * Application default home controller and default index method.
     */
	public function index(){
	    if (Auth::isLoggedIn()){
            header("Location: " . URL_PROJECT_PATH . "/bo");
        }else{
            $this->view('users/login');
        }
	}
	
}