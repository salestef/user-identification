<?php

class User extends Model
{
    /** @var int $id ID of the User */
    public $id;
    
    /** @var string $name Name of the User */
    public $name;

    /** @var string $lastName Last name of the User */
    public $lastName;

    /** @var string $email Email of the User */
    public $email;

    /** @var string $password User password */
    public $password;

    /** @var string $created Time of User registration */
    public $created;

    /**
     * User login
     * @param string $email User email
     * @param string $password User password
     * @return bool|User False if not found and not logged in, instance of User class with his data otherwise.
     */
    public function login($email, $password)
    {
        // Check does user with this credentials exists if not return to login
        $user = $this->userExists($email, $password);

        if (empty($user) || sizeof($user) > 1) {
            header("Location: " . URL_PROJECT_PATH . "/users/login?wrongCredentials");
            return true;
        }

        // Fill the User data
        if (!empty($user) && sizeof($user) === 1) {
            if (isset($user[0])) {
                $this->id = $user[0]->id;
                $this->name = $user[0]->name;
                $this->lastName = $user[0]->last_name;
                $this->email = $user[0]->email;
                $this->password = $user[0]->password;
                $this->created = $user[0]->created;
            }
        }

        return isset($user[0]) ? $this : false;
    }

    /**
     * User registration.
     * @param string $email User email
     * @param string $password User password
     * @param string $name Name of the User.
     * @param string $lastName Last name of the User.
     * @return bool If success return true otherwise redirect to registration.
     */
    public function register($email, $password, $name, $lastName)
    {
        // Check does user with this credentials exists if not return to registration
        if (!empty($this->userExists($email))) {
            header("Location: " . URL_PROJECT_PATH . "/users/register?exists");
        }else{
            // Insert new User
            $stmt = $this->db->prepare('INSERT INTO users (`name`, `last_name`, `email`, `password`, `created`) VALUES (?,?,?,?,NOW())');
            $user = $stmt->execute([$name, $lastName, $email, $password]);
            return $user;
        }
    }

    /**
     * Logic is separated because same code is used in user registration and login.
     * @param string $email User email
     * @param bool|string $password Optional User password
     * @return Object[] Matching users.
     */
    private function userExists($email, $password = false)
    {
        if (!$password) {
            $query = 'SELECT * FROM users WHERE email = :email';
        } else {
            $query = 'SELECT * FROM users WHERE email = :email AND password = :password';
        }
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':email', $email);

        if($password !== false){
            $stmt->bindParam(':password', $password);
        }

        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Get registered users in time descending order by created DB table field that represents time of User registration.
     * @return array All registered users.
     */
    public function getAllUsers()
    {
        $stmt = $this->db->prepare('SELECT * FROM users ORDER BY created DESC');
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

}