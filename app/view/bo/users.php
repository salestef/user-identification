<div class="bo-wrapper">
    <?php

    use JasonGrimes\Paginator;

    if (!empty($data['users'])) {
        $total = sizeof($data['users']);
        $boPagination = 3;
        $numOfPages = (int)(ceil($total / $boPagination));

        if (!isset($_GET['page'])) {
            $currentPage = 1;
        } else {
            $currentPage = (int)$_GET['page'];
            if ($currentPage <= 0 || $currentPage > $numOfPages) {
                $currentPage = 1;
            }
        }

        $usersIndexMax = $currentPage * $boPagination;
        $usersIndexMin = $usersIndexMax - $boPagination;
        $rest = $total % $boPagination;


        if ($usersIndexMax > $total && !empty($rest)) {
            $userToShow = array_slice($data['users'], ($rest * -1));
        } else {
            $userToShow = array_slice($data['users'], $usersIndexMin, $boPagination);
        }

        $urlPattern = URL_PROJECT_PATH . '/bo/users?page=(:num)';

        $paginator = new Paginator($total, $boPagination, $currentPage, $urlPattern);

        $lastUser = 0;

        foreach ($userToShow as $user) {

            ?>
            <div <?= $user->id === $_SESSION['user']['id'] ? "class='current-user'" : "class='just-user'" ?>
                <?php
                if ($lastUser === sizeof($userToShow) - 1) {
                    echo "id='last-user'";
                }
                ?>>
                <?php if ($user->id === $_SESSION['user']['id']) { ?>
                    <?= "<p> Current User</p>" ?>
                <?php } ?>
                <p> Name: <?= ucfirst($user->name) ?></p>
                <p> LastName: <?= ucfirst($user->last_name) ?></p>
                <p> Email: <?= $user->email ?></p>
                <p> Created: <?= $user->created ?></p>
            </div>
            <?php
            $lastUser++;
        }
        ?>
        <div class="pagination-bo"><?= $paginator ?></div>
        <?php
    } else {
        ?>
        <h3>There is no registered users!</h3>
        <?php
    }
    ?>
</div>
