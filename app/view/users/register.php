<form class="form-signin" action="<?= URL_PROJECT_PATH . '/users/register' ?>" method="post">
    <h1 class="h3 mb-3 font-weight-normal">Please register</h1>
    <label for="inputName" class="sr-only">Name</label>
    <input type="text" id="inputName" name="name" class="form-control" placeholder="Name" required autofocus></br>
    <label for="inputLastName" class="sr-only">Lastname</label>
    <input type="text" id="inputLastName" name="lastName" class="form-control" placeholder="Lastname" required></br>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus></br>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required></br>
    <button class="btn btn-lg btn-primary btn-mar" type="submit">Sign in</button></br>
    <a href="<?= URL_PROJECT_PATH . '/users/login' ?>">You all ready have an account!</a>
    <?php
    if(isset($_GET['exists'])){ ?>
        <div>
            <h5 style="color:orangered;">This email is taken!</h5>
        </div>
        <?php
    }
    ?>
</form>
