<form class="form-signin" action="<?= URL_PROJECT_PATH . '/users/login' ?>" method="post">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required
           autofocus></br>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required></br>
    <button class="btn btn-lg btn-primary btn-mar" type="submit">Sign in</button>
    </br>
    <a href="<?= URL_PROJECT_PATH . '/users/register' ?>" class="form-link">Don't have an account?</a>
    <?php
    if (isset($_GET['wrongCredentials'])) { ?>
        <div>
            <h5 style="color:orangered;">You entered wrong credentials! </h5>
        </div>
        <?php
    }
    ?>
</form>

