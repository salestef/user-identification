<nav class="navbar navbar-expand-md mb-4">
    <a class="navbar-brand" href="<?= URL_PROJECT_PATH . '/bo'?>">Back Office</a>
    <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= URL_PROJECT_PATH . '/bo/home'?>"">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= URL_PROJECT_PATH . '/bo/users'?>"">List all users</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= URL_PROJECT_PATH . '/users/logout'?>">Logout</a>
                    </li>
                </ul>
    </div>
</nav>

