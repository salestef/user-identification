<?php
include 'baseController.php';

/**
 * Class Controller extends BaseController class and only important difference between them is use of authorization.
 * Child controllers that extends this class are not reachable without authorization, registration and login.
 */
class Controller extends BaseController
{

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        parent::__construct();
        if (!Auth::isLoggedIn()) {
            header("Location: " . URL_PROJECT_PATH . "/users/login");
        }
    }


}