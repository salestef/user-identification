<?php

class Auth
{
    /**
     * Checks is session with user is created in other words is this user all ready logged in or not.
     * @return bool
     */
    public static function isLoggedIn(){
        return isset($_SESSION['user']) ? true : false;
    }
}